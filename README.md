# 科学的根拠のためのオープンアクセス文書の翻訳の集積所

 (A repository of open acccess document translations for scientific evidence)

* Lars Jørgensen, Peter C. Gøtzsche, Tom Jefferson「[ヒトパピローマウイルス (HPV) ワクチンの利益と害 : 臨床試験報告書の試験データのメタ分析とシステマティック・レビュー](HPV2020.md)」、2020年、CC-BY 4.0。

## 他のサイトでの公開物
### JapanCannabisAs 氏の翻訳
* 世界保健機関 「[WHO 薬物依存専門家委員会第41会期 報告書](https://drive.google.com/file/d/1WrIqxA16OCmfWcGDzUtnY5AI8W6jQoJg/view) (PDF)」、2018年。CC BY-NC-SA 3.0
* 世界保健機関 「[WHO 薬物依存専門家委員会第40 会期 報告書](https://drive.google.com/file/d/1yA8tNZV3Li7DmWmGnp9eZJ3vvbf3KbX9/view) (PDF)」、2018年。CC BY-NC-SA 3.0
* 「[曝露マージンアプローチを用いたアルコール、たばこ、大麻、その他の違法薬物の比較リスクアセスメント (抄録)](https://drive.google.com/file/d/1o4VI7bWdc6_cx8IZn_KEvbEOGzqxFM4w/view) (PDF)」、2015年。CC BY-NC-SA 4.0
* Robert Melamede「[ハームリダクション-大麻のパラドックス (日本語仮訳)](https://drive.google.com/file/d/17O6DTYpxIzs70lmJbmQ9KsCy4Uih2fZN/view) (PDF)」、2005年。CC-BY 2.0

### 未決定 氏の解説
* 未決定「[「サイケデリック系薬物の長期使用は、脳の構造と性格の違いに関わっている」のまとめ](https://note.com/medi_drug/n/n5565344e15cd)」、2015年。オープンソースではないが、複製ではない解説です。Not duplicate, Explanatory text about a article.

## 外部リンク
* [オープンアクセスのニュース](https://current.ndl.go.jp/taxonomy/term/724?page=1)、カレントアウェアネス・ポータル